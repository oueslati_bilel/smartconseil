import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {TokenStorageService} from './services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  constructor(public tokenStorage: TokenStorageService, public router: Router) {}
  canActivate(): boolean{
    if (!this.tokenStorage.getToken()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
