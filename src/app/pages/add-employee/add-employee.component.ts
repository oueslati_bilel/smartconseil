import { Component, OnInit } from '@angular/core';
import {Employee} from '../../model/employee.model';
import {EmployeeService} from '../../services/employee.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  submitted = false;
  employee = new Employee();
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
  }

  save(employee: Employee) {
    this.employeeService.createproduct(employee).subscribe(data => {
        console.log(data);
        this.employee = data;
        alert("Ajout avec succée");
        this.router.navigate(['dashboard']);
      },
      error => console.log(error));

  }
}
