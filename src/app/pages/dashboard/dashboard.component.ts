import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Employee} from '../../model/employee.model';
import {EmployeeService} from '../../services/employee.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  employee?: Employee[];
  constructor(private router: Router, private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }
  retrieveTutorials(): void {
    this.employeeService.getAll()
      .subscribe(
        data => {
          this.employee = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
  goToaddemployee(): void{
      this.router.navigate(['/addemployee']);
    }

  delete(id: any): void {
    this.employeeService.delete(id)
      .subscribe(
        response => {
          console.log(response);
          this.retrieveTutorials();
          this.router.navigate(['/dashboard']);
        },
        error => {
          console.log(error);
        });
  }

}
