import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {AddEmployeeComponent} from './pages/add-employee/add-employee.component';
import {AuthguardGuard} from './authguard.guard';

const routes: Routes = [
  { path: 'login' , component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'dashboard' , component: DashboardComponent , canActivate: [AuthguardGuard]},
  { path: 'addemployee' , component: AddEmployeeComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
