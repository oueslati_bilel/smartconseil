import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Employee} from '../model/employee.model';


const baseUrl = 'http://localhost:8081/api/employee';
const baseUrl1 = 'http://localhost:8081/api/addEmp';
const baseUrl2 = 'http://localhost:8081/api/delete';



@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>(baseUrl);
  }
  createproduct(employee: Employee) {
    return this.http.post<Employee>(baseUrl1, employee);
  }
  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl2}/${id}`);
  }
}
